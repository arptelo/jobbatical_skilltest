var express = require("express"),
data = require("./server/data");

var app = express();

app.get("/users", data.getUser);
app.get("/topActiveUsers", data.getTopActiveUsers);

app.get("*", function(req,res){
    res.status(404).end();
});

var port = process.env.PORT || 3030;

app.listen(port);
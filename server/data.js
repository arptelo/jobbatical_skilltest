var config = require("./config").pg,
knex       = require("knex")(config),
Treeize    = require("treeize"),
Promise    = require("bluebird"),
pageSize   = 3;  //Number of records for each page

exports.getUser = function(req, res) {
    var pUserRows         = knex("users").select("id", "name", "created_at").where("id", req.query.id).then();
    var pCompanyRows      = knex("teams").select("companies.id", "companies.created_at", "companies.name", "teams.contact_user").join("companies", "teams.company_id", "=", "companies.id").where("user_id", req.query.id).then();
    var pListingsRows     = knex("listings").select("id", "created_at", "name", "description").where("created_by", req.query.id).then();
    var pApplicationsRows = knex("applications").join("listings", "applications.listing_id", "=", "listings.id")
        .select("applications.id", "applications.created_at", "listings.id as listing:id", "listings.name as listing:name", "listings.description as listing:description", "applications.cover_letter")
        .where("user_id", req.query.id).then();
    
    Promise.all([pUserRows, pCompanyRows, pListingsRows, pApplicationsRows]).then(function(results){
        var tree = new Treeize();
        tree.grow(results[3]);
        var user = results[0][0];
        user.companies = results[1];
        user.createdListings = results[2];
        user.applications = tree.getData();
        return res.json(user);
    }).catch(function(err){
        return res.status(500).json({ error: err.message });
    });
};

exports.getTopActiveUsers = function(req, res){
    var pActiveUsersRows = knex("users").join("applications", "applications.user_id", "=", "users.id")
        .select("users.id", "users.created_at", "users.name").count("applications.id")
        .whereRaw("applications.created_at BETWEEN LOCALTIMESTAMP - INTERVAL '7 days' AND LOCALTIMESTAMP")
        .groupBy("users.id")
        .orderByRaw("COUNT(applications.id) DESC")
        .limit(pageSize)
        .offset(req.query.page)
        .then();
    var pLatestListingsRows = knex.from(function(){
        this.from("applications").join("listings", "applications.listing_id", "=", "listings.id")
        .select("applications.user_id", "listings.name").select(knex.raw("rank() over (partition by applications.user_id order by applications.created_at) as rank")).as("t1");
    }).select("user_id", "name as listings:name").where("rank","<=", 3);
    Promise.all([pActiveUsersRows, pLatestListingsRows]).then(function(results){
        var tree = new Treeize();
        tree.grow(results[1]);
        var latestListings = tree.getData();
        return res.json(merge_arrays(results[0],latestListings));
    }).catch(function(err){
        return res.status(500).json({ error: err.message });
    });
};

function merge_arrays(arr1,arr2){
    for (var index1 = 0; index1 < arr1.length; ++index1) {
        for (var index2 = 0; index2 < arr2.length; ++index2) {
            if(arr1[index1].id === arr2[index2].user_id){
                arr1[index1].listings = arrayOfOnlyObjectValues(arr2[index2].listings);
                break;
            }
        }
    }
    return arr1;
}

function arrayOfOnlyObjectValues(arrayOfObj){
    var arr = [];
    for(var i = 0; i < arrayOfObj.length; ++i){
        arr.push(arrayOfObj[i].name);
    }
    return arr;
}
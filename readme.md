# Endpoints for Skilltest

This is the backend JSON API for an internal tool for getting an overview on the most active users. Activity is a measure of applied listings.

## /topActiveUsers?page={pageNumber}

Returns user info, count of applications in the last week and the names of the 3 latest applied listings. If pageNumber is missing, returns all data.

## /users?id={user.id}

Takes user id and returns:
* user info;
* connected companies;
* listings created by the user;
* applications the user has made with the info of the listing the application is made to.

The parameter "user.id" is required.